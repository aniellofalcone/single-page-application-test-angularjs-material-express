## Read Git Flow Wiki [here](https://bitbucket.org/aniellofalcone/ng-seed/wiki/Git%20flow%20starting%20guide)


Main folders and files

* bower.json contains the list of web dependecies (jquery, bootstrap...)
* config/ contains configuration files (app, phonegap, secret deploy keys)
* src/ contains the source code (javascript, images, styles) of the app


## Installing

* In the project folder run:

```
#!console

npm install && bower install
```

That's it!


##To use

* To serve the project run:

```
#!console

npm start

```

It will serve on http://localhost:8000


```
#!console

Thanks! ;) Hope you enjoy ;)
```