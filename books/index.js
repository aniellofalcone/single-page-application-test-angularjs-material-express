/*jshint esversion: 6 */
"use strict";
var express = require('express');
var _ = require('underscore');
var books = express.Router();
var booksJSONOriginal = require("./books.json");

// ******************************
// GET HOME PAGE, PAGINATED AND FILTERED IF FILTERS ARE NOT NULL
// ******************************

books.get('/', function(req, res, next) {
    var _FILTER_category = req.query.category ? req.query.category.toLowerCase() : undefined;
    var _FILTER_name = req.query.name ? req.query.name.toLowerCase() : undefined;
    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        if (_FILTER_category && _FILTER_name) {
            return book.genre.category.toLowerCase() === _FILTER_category && book.genre.name.toLowerCase() === _FILTER_name
        } else if (_FILTER_category || _FILTER_name) {
            return book.genre.category.toLowerCase() === _FILTER_category || book.genre.name.toLowerCase() === _FILTER_name
        } else {
            return true
        }
    });
    var page = req.query.page || 0;
    var forpage = req.query.forpage || 4;
    var totalBooks = booksJSON.length;
    // ******************************
    // IF IT'S REQUESTED A BIGGER PAGE THAN JSON LENGTH RETURNS THE WHOLE JSON
    // INSTEAD OF PAGINATED
    // ******************************
    if (forpage >= totalBooks) {
        return res.status(200).json(booksJSON);
    }
    var pages = (totalBooks / forpage).toFixed(0);
    var skip = forpage * page;
    if (skip > 0) {
        booksJSON.splice(0, skip);
    }
    var b = booksJSON;
    var _books = booksJSON.splice(0, forpage);
    return res.status(200).json({
        books: _books,
        pages: pages,
        page: page
    });
});

var findTagsMatch = function(array, fromTags) {
    var t = [];
    for (var i = 0; i < array.length; i++) {
        for (var j = 0; j < fromTags.length; j++) {
            if (array[i] === fromTags[j]) {
                if (fromTags[j].length > 3) {
                    t.push(fromTags[j]);
                }
            }
        }
    }
    return _.unique(t);
};

books.get('/:bookId/similar', function(req, res, next) {
    // ******************************
    // SEARCH A BOOK
    // ******************************
    var confidence = 1;
    var currentBook = {};
    var currentBookID = req.params.bookId;
    var results = [];
    for (var i = 0; i < booksJSONOriginal.length; i++) {
        if (booksJSONOriginal[i].id === currentBookID) {
            currentBook = booksJSONOriginal[i];
            break;
        }
    }

    // ******************************
    // GET VARIABLES
    // ******************************

    var _genreCategory = currentBook.genre.category,
        _genreName = currentBook.genre.name,
        _author = currentBook.author.name,
        _tags = currentBook.description.split(" ").concat(currentBook.name.split(" "));


    // ******************************
    // START TO FIND STRICT TO LARGE FILTERS - LVL 1
    // ******************************

    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        var thisTags = book.description.split(" ").concat(book.name.split(" "));
        return book.genre.category === _genreCategory &&
            book.genre.name === _genreName &&
            book.author.name === _author &&
            findTagsMatch(thisTags, _tags).length >= confidence;
        return book.id != currentBookID && findTagsMatch(thisTags, _tags).length >= confidence;
    });
    if (booksJSON.length > 3) {
        res.status(200).json({
            strictLevel: 1,
            books: booksJSON
        });
    }

    // ******************************
    // START TO FIND STRICT TO LARGE FILTERS - LVL 2
    // ******************************

    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        var thisTags = book.description.split(" ").concat(book.name.split(" "));
        return book.genre.category === _genreCategory || (
            book.genre.name === _genreName &&
            book.author.name === _author &&
            findTagsMatch(thisTags, _tags).length >= confidence);
        return book.id != currentBookID && findTagsMatch(thisTags, _tags).length >= confidence;
    });
    if (booksJSON.length > 3) {
        res.status(200).json({
            strictLevel: 2,
            books: booksJSON
        });

    }
    // ******************************
    // START TO FIND STRICT TO LARGE FILTERS - LVL 3
    // ******************************

    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        var thisTags = book.description.split(" ").concat(book.name.split(" "));
        return book.genre.category === _genreCategory || book.genre.name === _genreName || (
            book.author.name === _author &&
            findTagsMatch(thisTags, _tags).length >= confidence);
        return book.id != currentBookID && findTagsMatch(thisTags, _tags).length >= confidence;
    });
    if (booksJSON.length > 3) {
        res.status(200).json({
            strictLevel: 3,
            books: booksJSON
        });
    }

    // ******************************
    // START TO FIND STRICT TO LARGE FILTERS - LVL 4
    // ******************************

    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        var thisTags = book.description.split(" ").concat(book.name.split(" "));
        return book.genre.category === _genreCategory || book.genre.name === _genreName || book.author.name === _author || (
            findTagsMatch(thisTags, _tags).length >= confidence);
        return book.id != currentBookID && findTagsMatch(thisTags, _tags).length >= confidence;
    });
    if (booksJSON.length > 3) {
        res.status(200).json({
            strictLevel: 4,
            books: booksJSON
        });
    }

    // ******************************
    // START TO FIND STRICT TO LARGE FILTERS - LVL 4
    // ******************************

    var booksJSON_base = Array.from(booksJSONOriginal, x => x);
    var booksJSON = _.filter(booksJSON_base, (book) => {
        var thisTags = book.description.split(" ").concat(book.name.split(" "));
        return book.genre.category === _genreCategory ||
            book.genre.name === _genreName ||
            book.author.name === _author ||
            findTagsMatch(thisTags, _tags).length >= confidence;
        return book.id != currentBookID && findTagsMatch(thisTags, _tags).length >= confidence;
    });
    res.status(200).json({
        strictLevel: 5,
        books: booksJSON
    });
});

module.exports = books;
