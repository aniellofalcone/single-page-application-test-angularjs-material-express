angular.module("ng-reedsytest", [
    'ngRoute',
    'ng-reedsytest.controllers',
    'ng-reedsytest.services',
    'ngMaterial',
    'angularUtils.directives.dirPagination',
    'underscore',
    'angularMoment',
    'ngSanitize'
])

// ******************************
// DEFINE ENDPOINT COSTANT FOR THE SERVICE
// ******************************

.constant("Gateway", {
    "endPoint": "http://localhost:8000" // localhost
})

// ******************************
// ROUTE PROVIDER CONFIG - ENABLED HTML5MODE
// ******************************

.config(["$routeProvider", "$locationProvider", function($routeProvider, $locationProvider) {
    $routeProvider

        .when('/', {
        templateUrl: 'app/templates/mainPage/mainPage.tpl.html',
    })

    .when('/bookdetails', {
        templateUrl: 'app/templates/bookDetails/bookDetails.tpl.html',
        controller: 'bookDetailsController'
    });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false,
        rewriteLinks: true
    });
}]);
