angular.module('ng-reedsytest.services', [])



.factory("BookService", ["$http", '$q', 'Gateway', '_', function bookService($http, $q, Gateway, _) {


    // ******************************
    // GET ALL BOOKS FROM JSON
    // ******************************

    function getAllBooks() {
        var deferred = $q.defer();
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?forpage=200'
            }).success(function(response) {
                deferred.resolve(response);
            })
            .error(function errorCallback(response) {
                console.log(response);
            });
        return deferred.promise;
    }

    // ******************************
    // GET PAGINATED BOOKS FROM JSON
    // ******************************

    function getPaginatedBooks(pageNumber) {
        var deferred = $q.defer();
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=' + pageNumber
            }).success(function(response) {
                deferred.resolve(response);
            })
            .error(function errorCallback(response) {
                console.log(response);
            });
        return deferred.promise;
    }


    // ******************************
    // GET ALL CATEGORIES FROM WHOLE JSON
    // ******************************

    function getAllCategories() {
        var deferred = $q.defer();
        var books = [];
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?forpage=200'
            }).success(function(response) {
                books = response;
                var categories = [];
                for (var i in books) {
                    categories.push(books[i].genre.category);
                }
                categories.sort();
                deferred.resolve(_.uniq(categories, true));
            })
            .error(function errorCallback(response) {
                console.log(response);

            });
        return deferred.promise;
    }

    // ******************************
    // GET ALL GENRES FROM WHOLE JSON
    // ******************************

    function getAllGenres() {
        var deferred = $q.defer();
        var books = [];
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?forpage=200'
            }).success(function(response) {
                books = response;
                var genres = [];
                for (var i in books) {
                    genres.push(books[i].genre.name);
                }
                genres.sort();
                deferred.resolve(_.uniq(genres, true));
            })
            .error(function errorCallback(response) {
                console.log(response);

            });
        return deferred.promise;
    }

    // ******************************
    // GET ALL BOOKS NAMES FROM WHOLE JSON - THEY'LL BE USED IN THE SEARCH
    // ******************************

    function getAllNames() {
        var deferred = $q.defer();
        var books = [];
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=0&forpage=200'
            }).success(function(response) {
                books = response;
                var names = [];
                for (var i in books) {
                    names.push(books[i].name);
                }
                names.sort();
                deferred.resolve(_.uniq(names, true));
            })
            .error(function errorCallback(response) {
                console.log(response);
            });
        return deferred.promise;
    }


    // ******************************
    // CALLS API FILTER FUNCTION TO GET FILTERED PAGINATED BOOKS JSON
    // ******************************

    function filterFor(genre, category) {
        var deferred = $q.defer();
        if ((genre != null || genre != undefined) && (category != null || category != undefined)) {
            $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=0forpage=4&category=' + category + '&name=' + genre
            }).then(function successCallback(response) {
                if (response.data.length <= 4) {
                    deferred.resolve(response.data);
                } else {
                    deferred.resolve(response.data.books)
                }
            }, function errorCallback(response) {
                console.log(response);
            });
        } else if ((genre != null || genre != undefined) && (category == null || category == undefined)) {
            $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=0&forpage=4&name=' + genre
            }).then(function successCallback(response) {
                if (response.data.length <= 4) {
                    deferred.resolve(response.data);
                } else {
                    deferred.resolve(response.data.books)
                }
            }, function errorCallback(response) {
                console.log(response);
            });
        } else if ((genre == null || genre == undefined) && (category != null || category != undefined)) {
            $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=0&forpage=4&category=' + category
            }).then(function successCallback(response) {
                if (response.data.length <= 4) {
                    deferred.resolve(response.data);
                } else {
                    deferred.resolve(response.data.books)
                }
            }, function errorCallback(response) {
                console.log(response);
            });
        } else {
            $http({
                method: 'GET',
                url: Gateway.endPoint + '/books?page=0'
            }).then(function successCallback(response) {
                deferred.resolve(response);
            }, function errorCallback(response) {
                console.log(response);
            });
        }
        return deferred.promise;
    }

    // ******************************
    // GET ALL SIMILAR FROM API AND PUT THREE RANDOM OF THEM IN A NEW ARRAY
    // ******************************

    function getSimilar(id) {
        var deferred = $q.defer();
        $http({
                method: 'GET',
                url: Gateway.endPoint + '/books/' + id + '/similar'
            }).success(function(response) {
                var threeSimilar = [];
                for (var i = 0; i < 3; i++) {
                    var randomId = Math.floor((Math.random() * response.books.length));
                    if (threeSimilar.length < 4) {
                        if (response.books[randomId].id == id) {
                            threeSimilar[i] = response.books[randomId + 1];
                            threeSimilar = _.uniq(threeSimilar);
                        } else {
                            threeSimilar[i] = response.books[randomId];
                            threeSimilar = _.uniq(threeSimilar);
                        }
                    }
                }
                deferred.resolve(threeSimilar);
            })
            .error(function errorCallback(response) {
                console.log(response);
            });
        return deferred.promise;
    }

    return {
        getAllBooks: getAllBooks,
        getPaginatedBooks: getPaginatedBooks,
        getAllCategories: getAllCategories,
        getAllGenres: getAllGenres,
        getAllNames: getAllNames,
        filterFor: filterFor,
        getSimilar: getSimilar
    };
}]);
