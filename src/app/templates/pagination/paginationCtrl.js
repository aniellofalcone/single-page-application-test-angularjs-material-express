angular.module("ng-reedsytest.controllers")

.controller('paginationController', function($scope, $rootScope, $http, BookService, $location) {

    // ******************************
    // VARIABLES DECLARATION
    // ******************************

    $rootScope.books = [];
    $rootScope.threeSimilar = [];
    $scope.similar = [];
    $scope.totalBooks = getTotalBooksNumber();
    $scope.booksPerPage = 4;
    $scope.totalPages = getTotalPages();
    $rootScope.showDetails = false;
    getResultsPage($rootScope.currentPage);
    $rootScope.currentPage = 0;

    // ******************************
    // GET THE TOTAL BOOKS NUMBER FROM API FUNCTION
    // ******************************

    function getTotalBooksNumber() {
        BookService.getAllBooks().then(function(res) {
            $scope.totalBooks = res.length;
        });
    }

    // ******************************
    // GET THE NUMBER OF PAGES FROM API FUNCTION
    // ******************************

    function getTotalPages() {
        setTimeout(function() {
            $scope.totalPages = (($scope.totalBooks / $scope.booksPerPage) - 1);
        }, 100);
    }

    // ******************************
    // GET THE CURRENT PAGE CONTENT FROM PAGINATED BOOKS ARRAY RETURNED BY API
    // ******************************

    function getResultsPage(pageNumber) {
        BookService.getPaginatedBooks($rootScope.currentPage).then(function(res) {
            $rootScope.books = res.books;
        });
    }

    // ******************************
    // CHANGE THE CURRENT PAGE VALUE AND PASSES IT TO THE GET RESULTS FUNCTION
    // ******************************

    $scope.nextPage = function() {
        $rootScope.currentPage += 1;
        if ($rootScope.currentPage > $scope.totalPages) {
            $rootScope.currentPage = 0;
            getResultsPage($rootScope.currentPage);
        } else {
            getResultsPage($rootScope.currentPage);
        }
    };

    // ******************************
    // CHANGE THE CURRENT PAGE VALUE  AND PASSES IT TO THE GET RESULTS FUNCTION
    // ******************************

    $scope.previusPage = function() {
        $rootScope.currentPage -= 1;
        if ($rootScope.currentPage < 0) {
            $rootScope.currentPage = ($scope.totalPages);
            getResultsPage($rootScope.currentPage);
        } else {
            getResultsPage($rootScope.currentPage);
        }
    };

    // ******************************
    // FUNCTION TO GET CLICKED BOOK DETAILS - IT SETS THE SELECTEDBOOK VARIABLE
    // IN THE ROOTSCOPE SO IT CAN BE ACCESSED IN THE BOOKDETAILS CONTROLLER AND
    // SET AS TRUE THE VALUE OF SHOWDETAILS SO BOOKDETAILS TEMPLATE BECOME VISIBLE
    // ******************************

    $rootScope.clicked = function(book) {
        $rootScope.selectedBook = book;
        BookService.getSimilar($rootScope.selectedBook.id).then(function(res) {
            $rootScope.threeSimilar = res;
        });
        $rootScope.showDetails = true;
    };

});
