angular.module("ng-reedsytest.controllers")

.controller('selectController', function($scope, $q, $timeout, BookService, $rootScope, $location) {

    // ******************************
    // VARIABLE DECLARATION FOR ALL THE FILTER FUNCTIONS
    // ******************************

    $scope.categories = [];
    $scope.genres = [];
    $scope.names = [];
    $scope.books = [];
    $scope.filteredBooks = [];
    $rootScope.disableButtons = false;
    $rootScope.noMatchFound = false;
    $rootScope.noMatch = '<h4> So sorry about that, but it seems you were looking for something I could not found! What about changing filters? ;) </h4>';
    $scope.filters = {
        Genre: null,
        Category: null
    };

    // ******************************
    // AUTOCOMPLETE SETTINGS
    // ******************************
    var self = this;
    self.simulateQuery = true;
    self.isDisabled = false;

    // ******************************
    // LIST OF BOOK VALUE/DISPLAY OBJECTS
    // ******************************
    $scope.names = loadAllNames();
    $scope.books = loadAllBooks();
    $scope.genres = getGenres();
    $scope.categories = getCategories();
    self.querySearch = querySearch;
    self.selectedItemChange = selectedItemChange;

    // ******************************
    // AUTOCOMPLETE SEARCH FUNCTION THAT CREATE A RANDOM TIMEOUT TO SIMULATE A REAL QUERY
    // ******************************

    function querySearch(query) {
        var results = query ? $scope.names.filter(createFilterFor(query)) : $scope.names,
            deferred;
        if (self.simulateQuery) {
            deferred = $q.defer();
            $timeout(function() {
                deferred.resolve(results);
            }, Math.random() * 1000, true);
            return deferred.promise;
        } else {
            return results;
        }
    }

    // ******************************
    // AUTOCOMPLETE FUNCTION FOR THE SELECTED BOOK
    // ******************************

    function selectedItemChange(item) {
        for (var i = 0; i < $scope.books.length; i++) {
            if (item.display == $scope.books[i].name) {
                $rootScope.selectedBook = $scope.books[i];
                $rootScope.showDetails = true;
            }
        }
    }

    // ******************************
    // GETS ALL NAMES FROM SERVICE FUNCTION AND SET THE NAMES ARRAY ELEMENTS
    // AND RETURNS VALUE AND DISPLAY FOR THE AUTOCOMPLETE INPUT
    // ******************************

    function loadAllNames() {
        BookService.getAllNames().then(function(res) {
            $scope.names = res;
            $scope.names = $scope.names.map(function(name) {
                return {
                    value: name.toLowerCase(),
                    display: name
                };
            });
        });
    }

    // ******************************
    // GETS ALL BOOKS FROM SERVICE FUNCTION AND SET THE BOOKS ARRAY ELEMENTS
    // ******************************

    function loadAllBooks() {
        BookService.getAllBooks().then(function(res) {
            $scope.books = res;
        });
    }

    // ******************************
    // CREATE A FILTER FOR THE QUERY SO AUTOCOMPLETE DISPLAYS FILTERED RESULTS ONLY
    // (IT WILL LOOK FOR ANY OCCASION OF THE LETTER NOT ONLY THE FIRST ONE)
    // ******************************

    function createFilterFor(query) {
        var lowercaseQuery = angular.lowercase(query);

        return function filterFn(book) {
            return (book.value.indexOf(lowercaseQuery) != -1);
        };
    }

    // ******************************
    // GETS ALL CATEGORIES FROM SERVICE FUNCTION AND SET THE CATEGORY ARRAY ELEMENTS
    // ******************************

    function getCategories() {
        BookService.getAllCategories().then(function(res) {
            $scope.categories = res;
        });
    }

    // ******************************
    // GETS ALL GENRES FROM SERVICE FUNCTION
    // ******************************

    function getGenres() {
        BookService.getAllGenres().then(function(res) {
            $scope.genres = res;
        });
    }

    // ******************************
    // ALLOWS USER TO FILTER THE WHOLE BOOKS ARRAY BY GENRE
    // AND PUSH IT IN THE VIEW - IF NONE IS FOUND RETURNS A MESSAGE
    // ******************************

    $scope.filterByGenre = function(genre) {
        $scope.filters.Genre = genre;
        BookService.filterFor($scope.filters.Genre, $scope.filters.Category).then(function(res) {
            $rootScope.books = res;
            $rootScope.disableButtons = true;
            if ($rootScope.books.length == 0) {
                $rootScope.noMatchFound = true;
            } else {
                $rootScope.noMatchFound = false;
            }
        });
    };

    // ******************************
    // ALLOWS USER TO FILTER THE WHOLE BOOKS ARRAY BY CATEGORY
    // AND PUSH IT IN THE VIEW - IF NONE IS FOUND RETURNS A MESSAGE
    // ******************************

    $scope.filterByCategory = function(category) {
        $scope.filters.Category = category;
        console.log("Filtri: ", $scope.filters);
        BookService.filterFor($scope.filters.Genre, $scope.filters.Category).then(function(res) {
            $rootScope.books = res;
            $rootScope.disableButtons = true;
            if ($rootScope.books.length == 0) {
                $rootScope.noMatchFound = true;

            } else {
                $rootScope.noMatchFound = false;
            }
        });
    };

    $scope.deleteFilters = function() {
        $scope.filters = {
            Genre: null,
            Category: null
        };
        BookService.getPaginatedBooks($rootScope.currentPage).then(function(res) {
            $rootScope.books = res.books;
        });
    };

});
